import os

from flask import Flask
from redis import Redis

app = Flask(__name__)
redis = Redis(host='redis', port=6379) 

@app.route('/example')
def example():
    redis.incr('hello_worlds')
    return 'This route has outputted a total of {} "hello worlds"'.format(int(redis.get('hello_worlds')))

if __name__ == "__main__": 
    app.run(host='0.0.0.0', debug=True)
