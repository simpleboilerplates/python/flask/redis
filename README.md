# Python > Flask > Redis boilerplate

## Setup:

1. Clone this repo 
2. Build the docker image
``` docker-compose build```
3. Start the docker image
``` docker-compose up ```

## Usage:

An example route is implemented on localhost:5000/example/.
Feel free to use this as an example to create awesome apps!