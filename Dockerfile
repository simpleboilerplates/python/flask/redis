FROM python:3.7.3-alpine3.9

LABEL MAINTAINER='TBD'

WORKDIR /usr/src/app

COPY . .

RUN apk update
RUN pip3 install --no-cache-dir -r requirements.txt

EXPOSE 5000
CMD python3 app.py
